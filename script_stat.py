#!/usr/bin/python

import argparse
import csv
import shutil
import time
import os
import numpy as np
from statistics import mean
from statistics import stdev

import clean_mitab
import converter_csv_asp
from graph import Graph

start = time.time()


def open_data(file: str):
    with open(file, "r") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        return list(reader)


# Parser
parser = argparse.ArgumentParser()
parser.add_argument("infile", type=str, help=".mitab input file", metavar='INFILE')
parser.add_argument("--tabfile", type=str, help="Pre existing tabulated file from converter.")
parser.add_argument("--tabfile2", type=str, help="Pre existing tabulated file from converter.")
parser.add_argument("--oldtab", action="store_true", help="If tabfile is w/ alias (no isoforms).")
parser.add_argument("--withoutCHEBI", action="store_true", help="Does not take into account the "
                    "molecules with ChEBI identification (non-protein molecules).")
parser.add_argument("--graph", default=None, choices=['degree', 'coef', 'both', 'stacked',
                                                      'scatter', None],
                    help="Create histograms")
parser.add_argument("--interac", type=str, default='none',
                    choices=['human', 'human-mouse', 'chicken', 'mouse', 'mouse-rat', 'dog',
                             'taurus', 'rat', 'sheep', 'pig', 'none', 'unknown'],
                    help="Taxon filter for the graph compression.")
parser.add_argument("--withoutzero", action="store_true", help="Does not take into account the "
                                                               "protein with a coef of zero.")
parser.add_argument("--bins", type=int, default=100, help="Choose the number of bins in the graph ("
                                                          "default: 100).")
parser.add_argument("--log", action="store_true", default=False, help="logarithm scale for y axis")
parser.add_argument("--subtab", type=str, default=None, help="coef (x) or coef interval (x-x), "
                                                             "create a sub tabulated file.")
parser.add_argument("--neighbour", default=False, action="store_true",
                    help="Take into account the nodes neighbours for the --subtab or --tree "
                         "options if True.")
parser.add_argument("--conn", action="store_true", help="Count the connected components of the "
                                                          "graph.")
parser.add_argument("--equi", action="store_true", help="Process the data in order to find the "
                                                        "equivalence group.")
parser.add_argument("--cyto", action="store_true", help="Generate a tabulated file with the coef "
                                                        "of each node, can be used for "
                                                        "visualization in Cytoscape.")
parser.add_argument("--tree", action="store_true", help="Create a tabulated and an asp file "
                                                        "without trees.")
parser.add_argument("--hub", type=float, default=None,
                    help="Threshold for the hub. ex: 0.01 if you want to remove the 1 percent most "
                         "connected nodes. Create a tabulated and an asp file without the hubs.")
parser.add_argument("--percent", type=str, default=None,
                    help="Percentage of the nodes we want to keep (between 0 and 1). "
                         "If 'all', does 10 to 90 percent files. Create a tabulated file and an "
                         "asp file w/ nodes w/ the smaller degree.")
parser.add_argument("--iso", action="store_true", help="Create a file with listing the isoforms "
                                                       "and their id.")
parser.add_argument("--ap", action="store_true", help="Detects the articulation points of the "
                                                      "graph.")
parser.add_argument("--degree", action="store_true", help="Give info on degree distribution.")
parser.add_argument("--origindata", type=str, default=None, help="Can be used to input the "
                                                                 "original data if the tabfile "
                                                                 "is a percent file.")
parser.add_argument("--test", type=int, help="Perform a given test")
args = parser.parse_args()

# Possibility to use an existing tabulated file
if not args.tabfile:
    print("=> Homogenization and filtration of protein interactions...")
    clean_files = clean_mitab.clean(args.infile, args.interac, 5, 6, args.withoutCHEBI)
    print("=> Homogenization and filtration of protein interactions: done")

    print("=> Conversion to ASP...")
    converted_files = converter_csv_asp.converter(clean_files[0])
    print("=> Conversion to ASP: done")

    tab_file = open_data(converted_files[1])
    tab_file_old = True
    tab_filename = args.infile
elif args.oldtab:
    tab_file = open_data(args.tabfile)
    tab_file_old = True
    tab_filename = args.tabfile
else:
    tab_file = open_data(args.tabfile)
    tab_file_old = False
    tab_filename = args.tabfile
    if args.tabfile2 is not None:
        tab_file2 = open_data(args.tabfile2)
        tab_file_old2 = False
        tab_filename2 = args.tabfile2

        mitab2 = Graph(tab_file2, tab_file_old2, args.infile, tab_filename2)
        mitab2.print_stat()

# Call the Graph class
mitab = Graph(tab_file, tab_file_old, args.infile, tab_filename)
mitab.print_stat()

# === TEST ======
if args.test is not None:

    def test1():
        # Retrieve the app neighbours, write them in a csv file
        mitab.set_neighbour_degree()
        app = {k: v for k, v in mitab.node_neighbour.items() if k.startswith('app_') or k == "app"}

        with open('matrixdb_Human_Human_171107_extended/app_neighbours.csv', 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter='\t')
            writer.writerow(['protein', 'neighbours'])
            for k, v in app.items():
                values = ""
                for val in v:
                    values += f"{val}, "
                values = values[:-2]
                writer.writerow([k, values])

        # Retrieve + write app_3 neighbours only
        app3 = [v for k, v in mitab.node_neighbour.items() if k == 'app_3'][0]
        app3_id = [(prot, ident) for ident, prot in mitab.id_name.items() if prot in app3]
        with open('matrixdb_Human_Human_171107_extended/app3_neighbours.csv', 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter='\t')
            for elt in app3_id:
                #print(elt)
                 writer.writerow([elt[0], elt[1]])

    def test2():
        mitab.set_neighbour_degree()
        print(f"# of app's neighbours: {len(mitab.node_neighbour['app_2'])}")

    def test3():
        kept_node = list(mitab.node_set - mitab2.node_set)
        mitab.write_tab_from_prot_list(kept_node)

    func_list = [test1, test2, test3]
    test_num = args.test - 1
    func_list[test_num]()


# ===============

# Isoforms
if args.iso:
    mitab.write_isoforms()
    print("Number of isoform edges: ", len(mitab.iso_tab_file))

# Connected components
if args.conn:
    mitab.set_connected_comp()
    print("Number of connected components: ", mitab.nb_connected_component)
    mitab.write_connected_comp()

# Equivalence
if args.equi:
    mitab.set_equivalence()
    print("Number of equivalence groups: {}".format(len(mitab.equivalence)))
    mitab.set_equi_surface()
    print("mean surface:\t{}".format(round(mean(mitab.equi_surface), 3)))
    print("std deviation:\t{}".format(round(stdev(mitab.equi_surface), 3)))
    print("max surface:\t{}".format(max(mitab.equi_surface)))
    sorted_list = sorted(mitab.equi_size_degree, key=lambda x:x[4])
    #for elt in sorted_list:
    #    print(elt[3], elt[4])
    mitab.write_equivalence()
    #mitab.print_interesting_equivalence()

# Degree
if args.degree:
    mitab.set_neighbour_degree()
    degree_list = [v for v in mitab.node_degree.values()]
    print("mean degree:\t{}".format(round(mean(degree_list), 3)))
    print("std deviation:\t{}".format(round(stdev(degree_list), 3)))
    print("max degree:\t{}".format(max(degree_list)))

# Cytoscape argument (tag each node w/ its cc)
if args.cyto:
    mitab.write_cyto_coef()

# Tree detection
if args.tree:
    mitab.set_connected_comp()
    tree = 0
    folder = mitab.choose_small_folder()
    tree_folder = folder + "/tree/"
    i = 1
    for component in mitab.connected_component:
        # create a tabulated file w/ component
        file_path, asp_path = mitab.write_tab_comp(component, folder, i, args.neighbour,
                                                   args.origindata)
        subgraph_tab = open_data(file_path)
        subgraph = Graph(subgraph_tab, False, args.infile, args.tabfile)
        if subgraph.is_tree():
            tree += 1
            # change directory
            new_file_path = tree_folder + os.path.basename(file_path)
            new_asp_path = tree_folder + os.path.basename(asp_path)
            shutil.move(file_path, new_file_path)
            shutil.move(asp_path, new_asp_path)
        i += 1
    print("The graph contains {} connected component(s) including {} tree(s).".format(
        mitab.nb_connected_component, tree))
    mitab.remove_tree(tree_folder)

# Hub detection
if args.hub is not None:
    mitab.write_tab_hub(args.hub)
    mitab.write_cyto_hub(args.hub)

# Small tabulated files creation
if args.percent is not None:
    if args.percent.lower() == "all":
        # TODO add argument to change the values of np.arrange()
        list_percent = np.arange(0.10, 0.90, 0.1)    # (a, b, c) from a to b w/ a step of c
        # contains the number of connected components of each tab file
        connected_data = []     # [xx, xx, xx]
        # contains the nb of nodes and the nb of interactions of each tab file
        info_tab = []           # [(x,y), (x,y), (x,y)]
        for percent in list_percent:
            result = mitab.write_tab_node_percent(percent)
            print("{} percent: done".format(percent))
            # add tuple (# nodes, # interactions)
            info_tab.append(result[1])
            # create Graph w/ new tab file
            new_file_path = open_data(result[0])
            new_tab_filename = os.path.basename(result[0])
            new_mitab = Graph(new_file_path, False, args.infile, new_tab_filename)
            # set the connected components
            new_mitab.set_connected_comp()
            nb_comp = new_mitab.nb_connected_component
            # add the nb of connected components to the list
            connected_data.append(nb_comp)
        print("info: ", info_tab)
        print("connected data: ", connected_data)

    else:
        percent = float(args.percent)
        mitab.write_tab_node_percent(percent)

# Articulation points
if args.ap:
    mitab.set_ap()
    print("The graph contains {} articulation points.".format(len(mitab.ap)))
    mitab.write_ap()

# Process the --subtab argument
if args.subtab is not None:
    mitab.set_neighbour_degree()
    mitab.set_local_coef()
    mitab.set_big_dict()
    s1 = args.subtab
    s2 = s1.split("-")
    if len(s2) > 1:
        coef1 = float(s2[0])
        coef2 = float(s2[1])
    else:
        coef1 = float(s2[0])
        coef2 = None
    mitab.write_tab_coef(coef1, coef2, args.neighbour)

# Graph options
if args.graph == 'both':
    mitab.graph_from_degree(zero=args.withoutzero, bins=args.bins, log=args.log)
    mitab.graph_from_coef(zero=args.withoutzero, bins=args.bins, log=args.log)
elif args.graph == 'degree':
    mitab.graph_from_degree(zero=args.withoutzero, bins=args.bins, log=args.log)
elif args.graph == 'coef':
    mitab.graph_from_coef(zero=args.withoutzero, bins=args.bins, log=args.log)
elif args.graph == 'stacked':
    mitab.graph_from_coef_stacked(zero=args.withoutzero, bins=args.bins, log=args.log)
elif args.graph == "scatter":
    mitab.graph_scatter()

end = time.time()
time_elapsed = end - start
print("Time elapsed: " + str(time.strftime('%H %M %S', time.gmtime(time_elapsed))))
