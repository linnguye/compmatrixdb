#!/usr/bin/python
#-*- coding: utf-8 -*-
from bubbletools import BubbleTree as bt
import csv
import random
import os
import shlex





def write_file(name: str, listmcs1: list):
    """
    write in an output file
    :param name: outfile name
    :param listmcs1: list of molecules
    """
    with open(name, "w", encoding='UTF-8') as ddl:
        for i in range(len(listmcs1)):
            ddl.write(str(listmcs1[i]) + "\n")
    ddl.close()
    assert os.path.isfile(name), "choicenodebble: The created file does not exist!"


def node(infile: str, check=''):
    """
    :param infile: bbl file
    :param check: 'all' to have the maximal concept
    :return: a list of nodes

    List the molecules of the nodes present in the powergraph
    """

    # Checking input arguments
    assert infile.endswith('.bbl'), "choicenodebbl: Input file extension is not '.bbl'"
    assert os.path.isfile(infile), "choicenodebbl: The input file does not exist!"

    # Recovering data of each power nodes:
    # def open_data(name):
    #     assert os.path.isfile(name), "choicenodebbl: The input file '" + name + "' does not exist!"
    #     with open(name, "r") as f:
    #         reader = csv.reader(f, delimiter="\t")
    #         return list(reader)

    # Nodes' molecules recovery
    tree = bt.from_bubble_file(infile)  # bubble tree
    list_data = []      # all data
    list_pnode = []     # powernodes
    for pnode in tree.powernodes():
        pwrn_data = tree.powernode_data(pnode)
        list_data.append([pnode, list(pwrn_data.contained_pnodes), list(pwrn_data.contained_nodes),
                          list(tree.powernodes_containing(pnode))])
        list_pnode.append(pnode)

    # Choose one node among all node
    list_check = []
    a = None
    if check == '':     # TODO a revoir, que contiennent les listes, etc.
        a = random.randint(0, len(list_pnode) - 1)
        try:
            print("Chosen node: " + list_pnode[a])
        except:
            print("a has a value of " + str(a) + " instead of a value between 0 and " + str(len(
                list_pnode)))
            a = random.randint(0, len(list_pnode))
            print("Chosen node: " + list_pnode[a])
        else:
            list_check = [a]
    elif check == 'all' and 'all' not in list_pnode:
        list_check = []
        for b in range(len(list_data)):
            if len(list_data[b][1]) > 0:
                list_check.append(list_pnode.index(list_data[b][0]))
    else:
        try:
            a = list_pnode.index(str(check))
            print("Chosen node: " + list_pnode[a])
        except:
            print(check)
            print("This powernode does not exist, try with '\\\"' in argument.")
            print(list_pnode)
            check = input("Enter the powernode:\n")
            try:
                a = list_pnode.index(str(check))
                print("Chosen node: " + list_pnode[a])
            except:
                print("Nope")
                assert a > 0
            else:
                list_check = [a]
        else:
            list_check = [a]

    list_all = []
    for a1 in range(len(list_check)):               # a1: index of elt in list_check
        a = list_check[a1]                          # a: elt in list_check = index of list_data
        list_pwrn = list_data[a][1][:]              # add pwrn_data.contained_pnodes
        list_pwrn.append(list_data[a][0])           # add pnode
        for j in range(len(list_data[a][3])):       # powernodes_containing(pnode)
            idx_pwrn1 = list_pnode.index(list_data[a][3][j])
            if len(list_data[idx_pwrn1][2]) > len(list_data[a][2]):
                list_pwrn.append(list_data[idx_pwrn1][0])

        # Retrieve the input file folder name
        i = len(infile) - 1
        while infile[i] != '/' and i >= 0:
            i -= 1
        folder = infile[:i]

        # Create folder for output
        folder += "/" + str(list_data[a][0]).replace('"','').replace(' ','_')
        i = 1
        while os.path.exists(folder):
            if i == 1:
                folder += "_1"
            else:
                j = len(folder)-1
                while j > 0 and folder[j] != '_':
                    j -= 1
                folder = folder[:j] + "_" + str(i)
            i += 1
        os.makedirs(folder)

        # Write in a file for each node
        list_filename = []
        for i in range(len(list_pwrn)):
            idx_pwrn2 = list_pnode.index(list_pwrn[i])
            filename = folder + "/list_of_" + str(list_data[idx_pwrn2][0]).replace('"','') + ".csv"
            write_file(filename, list_data[idx_pwrn2][2])
            list_filename.append(filename)
        list_all.append([list_data[a][0], list_pwrn, list_filename])
    return list_all


if __name__ == '__main__':
    test_file = "/run/media/linnguye/LINHCHI_16/M2BIG/STAGE/git/compmatrixdb/" \
                "matrixdb_CORE27_example_1/matrixdb_CORE27_example.bbl"
    data = bt.from_bubble_file(test_file)
    list_data = []      # all data
    list_pnode = []     # powernodes
    for pnode in data.powernodes():
        pwrn_data = data.powernode_data(pnode)
        list_data.append([pnode, list(pwrn_data.contained_pnodes), list(pwrn_data.contained_nodes),
                          list(data.powernodes_containing(pnode))])
        list_pnode.append(pnode)
    print(list_data)
    print(list_pnode)
