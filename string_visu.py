#!/usr/bin/env python

###############################################################################
# From a given list save the PNG image of the STRING network.
###############################################################################

import requests
import shutil
from time import sleep

def string_visu(protein_list: list,
                infilename: str,
                out_format: str = "image",
                method: str = "network",
                network_flavor: str = "evidence") -> str:
    string_api_url = "https://string-db.org/api"
    output_format = out_format
    method = method

    my_genes = protein_list

    # Construct the request
    request_url = f"{string_api_url}/{output_format}/{method}?"
    request_url += "identifiers=%s"
    request_url += f"&network_flavor={network_flavor}"

    # Call STRING and save output image
    request_url = request_url % "%0d".join(my_genes)
    output_file = "%s.png" % infilename

    r = requests.get(request_url, stream=True)
    if r.status_code == 200:
        with open(output_file, 'wb') as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)
    sleep(1)
    return output_file




if __name__ == '__main__':
    string_visu(['4932.YMR055C', '4932.YFR028C'])
