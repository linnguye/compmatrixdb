#!/usr/bin/python
#-*- coding: utf-8 -*-
import argparse
import bblINcsv
import choice_node_bbl


# Parser :
parser = argparse.ArgumentParser()
parser.add_argument("infile", type=str, help="Input file (.bbl).",
                    metavar='INFILE')
parser.add_argument("--annot", type=str, help="Annotation file.",
                    metavar='Annotation', default="annot/annot.csv")
parser.add_argument("score", type=float, help="Minimum enrichment score ("
                    "positive number).")
parser.add_argument("pv", type=float, help="p-value for functional annotation"
                    " (positive decimal number between 0 and 1).",
                    metavar='pvalue')
parser.add_argument("--withoutCHEBI", action="store_true", help="Does not "
                    "take into account the molecules with ChEBI identification"
                    " (non-protein molecules).")
parser.add_argument("fileref", type=str, help="Ref file for the bbl file "
                    "'_tab.csv'.", metavar='FileRef')   # TODO revoir argument
parser.add_argument("--pwrn", type=str, help="Chosen powernode from the "
                    "bbl file (write \'powernode name\' if powernode "
                    "name contains special characters)",
                    metavar='Powernode_choice', default='')
args = parser.parse_args()

# Retrieve the folder name:
i = len(args.infile) - 1
while args.infile[i] != '/' and i >= 0:
    i -= 1
folder = args.infile[:i]

# Choose the node
# Choice Node and functional annotation this node:
if args.pwrn != '':
    node = choice_node_bbl.node(args.infile, args.pwrn)
else:
    node = choice_node_bbl.node(args.infile)
bblINcsv.list_goterm(node, args.annot, args.score, args.pv, folder,
                    args.withoutCHEBI, args.fileref)
