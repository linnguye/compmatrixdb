#!/usr/bin/env python

###############################################################################
# For a given list of proteins single out only the interactions which have
# experimental evidences and print out their score
###############################################################################

import requests
import sys


def string_get_interaction(protein_list: list,
                           infilename: str,
                           out_format: str = "tsv",
                           method: str = "network") -> str:
    string_api_url = "https://string-db.org/api"
    output_format = out_format
    method = method

    my_genes = protein_list

    # Construct the request
    request_url = f"{string_api_url}/{output_format}/{method}?"
    request_url += "identifiers=%s" % "%0d".join(my_genes)

    output_file = "%s.tsv" % infilename

    try:
        r = requests.get(request_url)
    except requests.exceptions.RequestException as err:
        print(err)
        sys.exit(1)

    # Read and parse the results
    rfile = r.text.split("\n")

    for line in rfile[1:]:      # first line is the header
        l = line.split("\t")
        #print("\t".join(str(x) for x in l))     # print tsv
        if len(l) > 1:          # if list not empty
            p1, p2 = l[2], l[3]
            experimental_score = float(l[10])
            if experimental_score != 0:     # experimentally confirmed
                print("\t", "\t".join([p1,p2, "experimentally confirmed (prob. %.3f)" %
                                 experimental_score]))

    open(output_file, 'wb').write(r.content)
    return output_file


if __name__ == '__main__':
    l =["CDC42", "CDK1", "KIF23", "PLK1", "RAC2", "RACGAP1", "RHOA", "RHOB"]
    l2 = ['4932.YMR055C', '4932.YFR028C']
    string_get_interaction(l2)
