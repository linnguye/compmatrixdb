#!/usr/bin/python
#-*- coding: utf-8 -*-

import csv
import os


# Function to open mitab_data and file:
def open_data(file: str):
    with open(file, "r") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        return list(reader)

def clean(infile: str, taxon: str, a: int, b: int, chebi: bool):
    """
    :param infile: input file (.mitab)
    :param taxon: chosen taxon
    :param a: column number for alias A
    :param b: column number for alias B
    :param chebi: with (False) or without ChEBI (True)
    :return: the file name and the folder name
    """
    # Check input arguments and folders
    assert infile[-6:] == ".mitab", "The clean_mitab infile extension is not '.mitab'"
    assert os.path.isfile(infile), "mitab infile for clean_mitab does not exist!"
    assert os.path.isfile("annot/aliases.csv"), "The annot/aliases.csv file does not exist!"
    assert os.path.isfile("annot/taxon_aliases.csv"), "The annot/taxon_aliases.csv file does not "\
                                                      "exist!"
    assert os.path.isfile("annot/decomposables.csv"), "The annot/decomposables.csv file does not " \
                                                      "exist!"
    assert type(a) == int, "The first numerical argument for clean_mitab is not an integer"
    assert a > 0, "The first numerical argument for clean_mitab is not a nonzero positive number."
    a = a-1
    assert type(b) == int, "The second numerical argument for clean_mitab is not an integer"
    assert b > 0, "The second numerical argument for clean_mitab is not a nonzero positive number."
    b = b-1
    assert type(chebi) == bool, "The chebi argument for clean_mitab is not a boolean"

    # Open datafile:
    mitab_data = open_data(infile)

    # Check mitab data
    try:
        assert not (mitab_data[-1][a].isspace())
        assert (len(mitab_data[-1][a]) > 0)
    except:
        print("Last row does not have a value for the column '" + str(mitab_data[0][a]) + "'.")
        try:
            assert not (mitab_data[-2][a].isspace())
            assert (len(mitab_data[-2][a]) > 0)
        except:
            print("The second to last row does not have the same length as the first row.")
        else:
            print("The last row is deleted.")
            mitab_data = mitab_data[:-1]

    # Open other datafiles
    data_aliases = open_data("annot/aliases.csv")
    data_taxon = open_data("annot/taxon_aliases.csv")
    data_decomp = open_data("annot/decomposables.csv")

    # Check taxon
    list_taxon = ["human-mouse", "mouse-rat", "none"]
    tax = []        # chosen taxon(s)
    for i in range(len(data_taxon)):
        list_taxon.append(data_taxon[i][0])     # add all the taxons from taxon_aliases.csv
    try:
        taxon in list_taxon
    except:
        print("The taxon is not valid.")
    else:
        '''Possible taxons: chicken, human, dog, taurus, sheep, pig, mouse, rat, human-mouse, 
        mouse-rat, none, unknown '''
        if taxon == "human-mouse":
            tax = ["human", "mouse"]
        elif taxon == "mouse-rat":
            tax = ["mouse", "rat"]
        # elif taxon == "none":
        #     tax = ["none"]
        else:
            tax = [taxon]

    # Check the mitab file
    assert mitab_data[0][0] == "#Unique identifier for interactor A", \
        "The first column name is not '#Unique identifier for interactor A'"
    assert mitab_data[0][1] == "Unique identifier for interactor B", \
        "The column 2 name is not 'Unique identifier for interactor B'"
    assert mitab_data[0][2] == "Alternative identifier for interactor A", \
        "The collumn 3 name is not 'Alternative identifier for interactor A'"
    assert mitab_data[0][3] == "Alternative identifier for interactor B", \
        "The column 4 name is not 'Alternative identifier for interactor B'"
    assert mitab_data[0][a] == "Aliases for A", "The column number for 'Aliases for A' is not " \
                                                "correct"
    assert mitab_data[0][b] == "Aliases for B", "The column number for 'Aliases for B' is not " \
                                                "correct"
    assert a <= len(mitab_data[0]), "The column number for Aliases A is higher than the row length."
    assert b <= len(mitab_data[0]), "The column number for Aliases B is higher than the row length."
    assert mitab_data[0][9] == "NCBI Taxonomy identifier for interactor A", \
        "The column 10 name is not 'NCBI Taxonomy identifier for interactor A'"
    assert mitab_data[0][10] == "NCBI Taxonomy identifier for interactor B", \
        "The column 11 name is not 'NCBI Taxonomy identifier for interactor B'"

    # Clean mitab_data
    data_integrin = []         # contain integrin mitab_data
    for i in range(1, len(mitab_data)):
        for j in range(len(mitab_data[i])):
            if mitab_data[0][j] == "Aliases for A" or mitab_data[0][j] == "Aliases for B":
                k = 0
                while k < len(mitab_data[i][j]) and mitab_data[i][j][k] != "|":
                    k += 1
                if k != len(mitab_data[i][j]):
                    mitab_data[i][j] = mitab_data[i][j][:k]     # keep the first part of the alias
            # delete unnecessary part(s) of the name
            if mitab_data[i][j][:10] == "uniprotkb:":
                mitab_data[i][j] = mitab_data[i][j][10:]
            elif mitab_data[i][j][:7] == 'chebi:"' and mitab_data[i][j][-1] == '"':
                mitab_data[i][j] = mitab_data[i][j][7:-1]
            elif mitab_data[i][j][:9] == "matrixdb:":
                mitab_data[i][j] = mitab_data[i][j][9:]
            if mitab_data[i][j][-13:] == "(short label)":
                mitab_data[i][j] = mitab_data[i][j][:-13]
            elif mitab_data[i][j][-11:] == "(gene name)":
                mitab_data[i][j] = mitab_data[i][j][:-11]
            # replace the name
            for ka in range(len(data_aliases)):
                if mitab_data[i][j] == data_aliases[ka][1] \
                        and (mitab_data[0][j] == "Aliases for A"
                        or mitab_data[0][j] == "Aliases for B"):
                    mitab_data[i][j] = data_aliases[ka][0]
            for kt in range(len(data_taxon)):
                if mitab_data[i][j] == data_taxon[kt][1] \
                        and(mitab_data[0][j] == "NCBI Taxonomy identifier for interactor A"
                        or mitab_data[0][j] == "NCBI Taxonomy identifier for interactor B"):
                    mitab_data[i][j] = data_taxon[kt][0]
            for kd in range(len(data_decomp)):
                if mitab_data[i][j] == data_decomp[kd][0] \
                        and (mitab_data[0][j] == "Aliases for A"
                        or mitab_data[0][j] == "Aliases for B"):
                    mitab_data[i][j] = data_decomp[kd][1]
                    data_integrin.append(mitab_data[i])
                    mitab_data[i][j] = data_decomp[kd][2]
            for jj in range(2, 4):
                if mitab_data[i][jj][:4] == "ebi:":
                    mitab_data[i][jj-2] = mitab_data[i][jj]

    # Add additional integrins
    for i in range(len(data_integrin)):
        mitab_data.append(data_integrin[i])

    # Create a list to write the file
    assert len(tax) > 0                             # check taxon list
    data_write = []
    for i in range(len(mitab_data)):
        # [A id, B id, A alias, B alias, A taxon, B taxon]
        donnee = [mitab_data[i][0], mitab_data[i][1], mitab_data[i][a], mitab_data[i][b],
                 mitab_data[i][9], mitab_data[i][10]]
        # chebi option + check if the line is already in data_write
        if ((mitab_data[i][0][:6] != "CHEBI:" and mitab_data[i][1][:6] != "CHEBI:") or
                chebi is False) and mitab_data[i] != [] and (donnee not in data_write):
            if tax == ["none"]:
                data_write.append(donnee)
            else:
                for taxn in range(len(tax)):        # taxon filter
                    if ((mitab_data[i][9] == tax[taxn] or mitab_data[i][10] == tax[taxn])
                            and (donnee not in data_write)):
                        data_write.append(donnee)

    # Remove edge between a node and itself + remove redundant interaction lines
    for line in data_write:
        if line[0] == line[1] or line[2] == line[3]:
            data_write[:] = [x for x in data_write if x != line]
        line_bis = [line[1], line[0], line[3], line[2], line[5], line[4]]
        if line_bis in data_write:
            data_write[:] = [x for x in data_write if x != line_bis]


    # Retrieve input filename
    i = len(infile) - 1
    while infile[i] != '/' and i >= 0:
        i -= 1      # index of filename first char
    # Create folder with appropriate name (no duplicate)
    if chebi:
        folder = infile[i+1:-6] + "_Without_CHEBI"
    else:
        folder = infile[i+1:-6]
    i = 1
    while os.path.exists(folder):
        if i == 1:
            folder += "_1"
        else:
            j = len(folder)-1
            while j > 0 and folder[j] != '_':
                j -= 1
            folder = folder[:j] + "_" + str(i)
        i += 1
    os.makedirs(folder)

    # Writing in new file of csv:
    filename = folder + "/" + infile[:-6] + ".csv"
    with open(filename, "w", encoding='UTF-8') as ddl:
        for i in range(1, len(data_write)):
            writing = ""
            for j in range(len(data_write[i])):
                writing += str(data_write[i][j]) + "\t"
            ddl.write(writing[:-1] + "\n")
    ddl.close()
    return filename, folder
