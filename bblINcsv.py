#!/usr/bin/python
#-*- coding: utf-8 -*-
import choice_node_bbl
import uniprot_david
import search_david
import enrichINlist
import os


def list_goterm(node, annot, score, pv, folder, chebi, tab_file):
    """

    :param node: list of [pnode, list_pwrn, list_filename]
    :param annot: default: annot/annot.csv
    :param score: minimum enrichment score
    :param pv: p-value
    :param folder: folder name
    :param chebi: boolean, if True --> without ChEBI molecules
    :param tab_file: tabulated file from the conversion

    Automates the functional annotation of the nodes in a powergraph
    """
    conclusion = [["NODE:", "Number protein for David:", "Number GOTERM:", "List of GOTERM:"]]
    list_node_david = []
    for max_node in range(len(node)):
        comparison = [["Power node:", "Number protein for David:", "Number GOTERM:",
                       "Protein delete:", "List of GOTERM", "Commentary:"]]
        best_list = [1, 0, 0]
        node_init = 1
        for n in range(len(node[max_node][2])):
            assert os.path.isfile(tab_file), "Ref file for uniprot_david does not exist!"
            idDavid = uniprot_david.list_id(node[max_node][2][n], tab_file)
            # Data of protein delete:
            delete = ''
            for i in range(len(idDavid[2])):
                delete += str(idDavid[2][i]) + ' '
            list_david = search_david.research(idDavid[1], annot, "UNIPROT_ACCESSION", 
                                               list_node_david)
            list_node_david = list_david[2]
            length_list = 0
            list_goterm = []
            commentary = ''
            if list_david[1]:
                for i in range(len(list_david[0])):
                    enriched = enrichINlist.enriched(list_david[i][0][0], 
                                node[max_node][1][n], score, pv)
                    length_list += enriched[0]
                    list_goterm.append(enriched[1])
                if length_list == 0:
                    commentary += "No enrichment obtained for the power node " + \
                                  node[max_node][1][n]
                else:
                    commentary += "Enrichment has resulted in a list of " + str(length_list) \
                                  + " GOTERM for the power node: " + node[max_node][1][n]
            else:
                commentary += "David don't provide of result for a list of " + str(len(idDavid[0]))\
                              + " molecule in the powernode: " + node[max_node][1][n]
                list_goterm = [[]]
            if node[max_node][1][n] == node[max_node][0]:
                node_init = n + 1
            comparison.append([node[max_node][1][n], len(idDavid[0]),
                               length_list, delete, list_goterm[0], commentary])
            if length_list > best_list[1] or n == 0:
                best_list = [n + 1, length_list, len(list_david[0])]
        
        # Conclusion messages of the comparison
        if chebi is False:      # TODO default value for chebi ? False or None
            commentary2 = "With the ChEBI molecules, an annotation score of " + str(score) + \
                          " and a p-value of " + str(pv) + ", the result is: \t\t\t\t"
        else:
            commentary2 = "Without the ChEBI molecules and with an annotation score of " \
                          + str(score) + " and a p-value of " + str(pv) \
                          + ", the result is: \t\t\t\t"
        if best_list[0] < node_init:
            commentary2 += "The chosen powernode must be cut before, to level of " \
                           + comparison[best_list[0]][0] + " to have a maximum annotation. "
        elif best_list[0] > node_init:
            commentary2 += "The chosen powernode is too small to have a maximum annotation, " \
                           "for that " + comparison[node_init][0] + " must be cut to the level of "\
                           + comparison[best_list[0]][0] + " whether " \
                           + str(best_list[0] - node_init) + " level above."
        elif best_list[0] == node_init and best_list[0] != 0:
            commentary2 += "The chosen powernode " + comparison[node_init][0] + " has obtained " \
                           "the largest annotation list. "
        elif best_list[0] == node_init and best_list[0] == 0:
            if comparison[best_list[0]][1] == 0:
                commentary2 += "No result was obtained because no enrichment was found. "
            commentary2 += "The chosen powernode is not cut."

        # Writing conclusion in file:
        # TODO changer nom du fichier de sortie (RESULAT_FINAL...)
        with open(str(folder) + "/RESULTAT_FINAL_" + str(node[max_node][0]).
                replace('"', '').replace(' ', '_') + ".csv", "w", encoding='UTF-8') as ddl:
            ddl.write(commentary2 + "\n\n")
            ddl.write("The chosen powernode is: \t" + node[max_node][0] + "\t\t\t\n\n")
            for i in range(len(comparison)):
                writing = ""
                for j in range(len(comparison[i])):
                    writing += str(comparison[i][j]) + "\t"
                ddl.write(writing[:-1] + "\n")
        ddl.close()
        donnee = comparison[node_init][:3]
        donnee.extend(comparison[node_init][4])
        conclusion.append(donnee)

    # Write the summary of all nodes in an output file:
    # TODO changer le nom du fichier de sortie
    with open(str(folder) + "/Conclusion_Node.csv", "w", encoding='UTF-8') as f:
        for i in range(len(conclusion)):
            writing = ""
            for j in range(len(conclusion[i])):
                writing += str(conclusion[i][j]) + "\t"
            f.write(writing[:-1] + "\n")
        f.close()
