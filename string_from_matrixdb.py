#!/usr/bin/env python

###############################################################################
# Retrieve the nodes from MatrixDB and search for annotation in STRING
# - map the identifiers into string identifiers
# - produce visualization for the STRING networks
#       - full annotation
#       - physical interaction only
# - produce the corresponding interaction tables
# - compare the two networks and produce a difference network
#       + the corresponding difference table
###############################################################################

import argparse
import os

from string_mapping import string_mapping as s_mapping
from string_visu import string_visu as s_visu
from string_get_interaction import string_get_interaction as s_interaction
# from string_get_difference import string_get_difference as s_diff

# Argument parser
parser = argparse.ArgumentParser()
parser.add_argument("proteinlist", type=argparse.FileType('r'),
                    help="List of proteins to be searched in STRING.")
parser.add_argument("--species", type=str, default=None, help="NCBI taxon identifiers")
parser.add_argument("--mapping", action="store_true", default=None,
                    help="Mapping of the protein names only.")
parser.add_argument("--visu", action="store_true", default=None,
                    help="Visualization of the networks only.")
parser.add_argument("--inter", action="store_true", default=None,
                    help="Retrieve interactions table only.")
parser.add_argument("--diff", type=argparse.FileType('r'), default=None,
                    help="Network file (tabulated file) used to compare te result found with the "
                         "STRING database. Produce the difference network and the difference "
                         "table from two networks (ex: MatrixDB and STRING).")
args = parser.parse_args()

# Parse protein list
prot_list = []                 # contain protein name
infilename = os.path.splitext(args.proteinlist.name)[0]

for line in args.proteinlist:
    prot_list.append(line.rstrip())
print("Original protein list: ", ", ".join(str(x) for x in prot_list))

# Protein name mapping
mapped_list, initial_list = s_mapping(prot_list)
print("Mapped protein: ", ", ".join(str(x) for x in mapped_list))

if args.mapping is None:    # code not run if --mapping argument
    # Generate network visualization
    if args.inter is None:
        output_image = s_visu(mapped_list, infilename)
        print(f"Created file: {output_image}\n")

    # Generate network tabulated file
    if args.visu is None:
        output_tab = s_interaction(mapped_list, infilename)
        print(f"Created file: {output_tab}\n")

    # TODO comparison of the tabulated files
    # Compare 2 network files and produce the difference table
    if args.diff is not None:
        print(f"Compare {args.diff.name} with STRING annotation.")
        #for line in args.diff:
        #    print(line)
