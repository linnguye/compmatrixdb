#!/usr/bin/python
#-*- coding: utf-8 -*-
import csv
import os


def open_data(file: str):
    with open(file, "r") as data_file:
        reader = csv.reader(data_file, delimiter="\t")
        return list(reader)


def converter(infile: str):
    """
    :param infile: cleaned mitab file (.csv)
    :return: an ASP file (.lp) and a tabulated file

    Convert a mitab file into a lp file (ASP language).
    """

    # Checking input arguments
    assert type(infile) == str, "Converter: the input file is not a string"
    assert infile.endswith('.csv'), "Converter: the input file extension is not '.csv'"
    assert os.path.isfile(infile), "Converter: the input file does not exist!"
    
    # Retrieving the output filename
    asp_filename = infile[:-4] + ".lp"

    # Retrieving full_data
    full_data = open_data(infile)
    
    # Writing the interactions between two molecules in an ASP file
    ab_data = []        # data for A and B interactor for each line
    with open(asp_filename, "w") as f:
        for i in range(1, len(full_data)-1):
            donnee = []  # TODO nom variable a changer
            # keep the complete lines
            if full_data[i][2] != '' and full_data[i][2] != '-':
                # check last character
                if full_data[i][2][-1] == ' ':
                    full_data[i][2] = full_data[i][2][:len(full_data[i][2])-1]
                # [A alias, A id, A taxon]
                donnee.append([full_data[i][2], full_data[i][0], full_data[i][4]])
                if full_data[i][3] != '' and full_data[i][3] != '-':
                    # same for B: [B alias B id, B taxon]
                    if full_data[i][3][-1] == ' ':
                        full_data[i][3] = full_data[i][3][:len(full_data[i][3])-1]
                    donnee.append([full_data[i][3], full_data[i][1], full_data[i][5]])
                    # ['edge', A alias, B alias, A id, B id, A taxon, B taxon]
                    ab_data_line = ["edge", donnee[0][0], donnee[1][0], donnee[0][1], donnee[1][1],
                                    donnee[0][2], donnee[1][2]]
                    ab_data.append(ab_data_line)
                    asp_line = 'edge("' + full_data[i][2] + '","' + full_data[i][3] + '").'
                    f.write(asp_line + "\n")
        f.close()

    # Writing in a tabulated file
    tab_filename = infile[:-4] + "_tab.csv"
    if len(ab_data) != 0:
        with open(tab_filename, "w", encoding='UTF-8') as f:
            for i in range(len(ab_data)):
                writing = ""
                for j in range(len(ab_data[i])):
                    writing += str(ab_data[i][j]) + "\t"
                f.write(writing[:-1] + "\n")
        f.close()

    return asp_filename, tab_filename
